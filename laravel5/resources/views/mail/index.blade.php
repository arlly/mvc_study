@extends('layouts.app')

@section('content')
<div class="container">

  <h2>メール送信</h2>
  
  {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
{!! Form::open(array('files' => true)) !!}
  
    <div class="form-group">
      {!! Form::label('name', 'お名前:') !!}
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('email', 'メールアドレス:') !!}
      {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('comment', 'コメント:') !!}
      {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
    </div>
 
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
{!! Form::close() !!}
</div>
@endsection
