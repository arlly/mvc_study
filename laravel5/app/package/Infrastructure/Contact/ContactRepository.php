<?php
namespace App\Package\Infrastructure\Contact;

use App\Package\Domain\Models\Contact\ContactRepositoryInterface;
use App\package\Infrastructure\Eloquents\Contact;


class ContactRepository implements ContactRepositoryInterface
{

    protected $eloquent;

	/**
	 * @param ShipStore $ShipStore
	 */
	public function __construct(Contact $contact)
	{
		$this->eloquent = $contact;
	}

	public function get(int $id)
	{
	    if (! is_numeric($id))
	    {
	        throw new \RuntimeException();
	    }
	    return $this->eloquent->findOrFail($id);

	}

	public function getList()
	{
	    return $this->eloquent->orderBy('id')->paginate(100);
	}

	public function update(int $id, $data)
	{

	}

	public function create($data)
	{
	    $model = $this->eloquent->create($data);
	    if (isset($model->id)) {
	        return $model->id;
	    }
	    return null;

	}

	public function delete(int $id)
	{
	    if (! is_numeric($id))
	    {
	        throw new \RuntimeException();
	    }

	    return $this->eloquent->find($id)->delete();

	}
}