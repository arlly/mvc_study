<?php

namespace App\package\Domain\Exception;

use League\Flysystem\Exception;

class PredictionException extends Exception {
	public function __construct($message, $code = 0, Exception $previous = null) {
		parent::__construct ( $message, $code, $previous );
	}
}