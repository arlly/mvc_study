<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Mailform;

class MailController extends Controller
{
    /**
	 * Index
	 */
	public function index() {
		
		return view('mail/index');
	}
	
	/**
	 * store
	 */
	public function store(Request $request) {
			
		$rules = [
				'name'    => 'required',
				'email'   => 'required|email',
				'comment' => 'required',
		];
		
		$this->validate($request, $rules);
		
		$inputs = $request->all();
		
		//DBに登録
		Mailform::create($inputs);
		
		\Mail::send('mail.email', $inputs, function($message) use ($inputs){ 
			$message->to($inputs["email"], $inputs["name"])->subject('送信ありがとうございます');
		});
		
		return redirect('mail/complete');
	}
	
	/**
	 * complete
	 */
	public function complete(){
		return view('mail/complete');
	}
}
