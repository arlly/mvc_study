<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


	Route::get('/Hello', function () {
		echo "Hello! World";
	});


	/**
	 * Mail
	 */
	Route::post('mail/index',   'MailController@store');
	Route::get('mail/index',    'MailController@index');
	Route::get('mail/complete', 'MailController@complete');

	/**
	 * Contact
	 */
	Route::resource('contacts', 'ContactController', ['only' => ['index', 'store']]);