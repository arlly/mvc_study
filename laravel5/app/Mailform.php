<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailform extends Model
{
    //
	protected $fillable = [
			'name', 'email', 'comment',
	];
}
